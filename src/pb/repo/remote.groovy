package pb.repo

def pushTags() {
  return sh(script: "git push --tags", returnStdout: true)?.trim()
}