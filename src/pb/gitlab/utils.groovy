package pb.gitlab

def isFinalBuildStatus(status) {
  return (status == 'canceled') || (status == 'success') || (status == 'failed')
}

def getCurrentBranch() {
  return env.gitlabBranch
}

def isCurrentBranchContaining(branchName) {
  return getCurrentBranch().contains(branchName)
}
