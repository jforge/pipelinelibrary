#!/usr/bin/env groovy

def call(Map input = [:]) {
  def config = [repoUrl: '', branch: '*/master', credentialsId: '', userName: '', userEmail: '']
  config << input

  echo "Simple checkout with ${config}"

  checkout changelog: false, poll: false,
    scm: [$class: 'GitSCM',
      branches: [[name: "${config.branch}"]],
      doGenerateSubmoduleConfigurations: false,
      extensions: [
        pruneTags(true),
        [$class: 'PruneStaleBranch'],
        [$class: 'LocalBranch'],
        [$class: 'CloneOption', depth: 1, noTags: false, reference: '', shallow: true],
        [$class: 'UserIdentity', email: "${config.userEmail}", name: "${config.userName}"]
      ],
      submoduleCfg: [],
      userRemoteConfigs: [
        [credentialsId: "${config.credentialsId}", url: "${config.repoUrl}"]]
    ]
}
