#!/usr/bin/env groovy

def call(steps = [], status = 'pending') {
  def utils = new pb.gitlab.utils()

  for (step in steps) {
    updateGitlabCommitStatus name: step, state: status
  }

  if (utils.isFinalBuildStatus(status)) {
    steps = []
  }
}
