#!/usr/bin/env groovy

def call() {
  def utils = new pb.repo.local()
  
  if (utils.getTags()) {
    return true
  }
  return false
}