#!/usr/bin/env groovy

def splitIntoPartitions(input, partitionSize) {
  def partitions = []
  partitions.add([:])

  def partition = 0
  def elementCount = 0;

  for (element in input) {
    partitions[partition][element.key] = element.value
    elementCount++
    if (elementCount == partitionSize) {
      partitions.add([:])
      partition++
      elementCount = 0
    }
  }
  if (partitions.last().size() == 0) {
    partitions.removeLast()
  }
  return partitions
}
