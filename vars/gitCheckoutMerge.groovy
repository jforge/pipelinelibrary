#!/usr/bin/env groovy

def call(Map input = [:]) {
  def config = [repoUrl: '', branch: '*/master', credentialsId: '', userName: '', userEmail: '',
                targetBranch: '*/master', targetRemote: 'origin',
                ffMode: 'NO_FF', mergeStrategy: 'DEFAULT']
  config << input

  echo "Merge checkout with ${config}"

  checkout changelog: true, poll: true, scm: [
    $class: 'GitSCM',
    branches: [[name: "${config.branch}"]],
    doGenerateSubmoduleConfigurations: false,
    extensions: [
      pruneTags(true),
      [$class: 'PruneStaleBranch'],
      [$class: 'LocalBranch'],
      [$class: 'CloneOption', depth: 30, noTags: false, reference: '', shallow: false],
      [$class: 'UserIdentity', email: "${config.userEmail}", name: "${config.userName}"],
      [$class: 'PreBuildMerge', options: [
        fastForwardMode: "${config.ffMode}",
        mergeRemote: "${config.targetRemote}",
        mergeStrategy: "${config.mergeStrategy}",
        mergeTarget: "${config.targetBranch}"]],
    ],
    submoduleCfg: [],
    userRemoteConfigs: [[credentialsId: "${config.credentialsId}", url: "${config.repoUrl}"]]
  ]
}
