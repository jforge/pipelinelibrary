#!/usr/bin/env groovy

def call(defaultBranch = '') {

  if (env.gitlabSourceBranch) {
    return env.gitlabSourceBranch
  }

  return defaultBranch
}
