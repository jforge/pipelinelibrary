#!/usr/bin/env groovy

def call() {
  def repoUtils = new pb.repo.local()

  def commitMsg = repoUtils.getCurrentCommitMessage()
  return commitMsg.contains('CI: tag=')
}