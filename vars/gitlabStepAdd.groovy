#!/usr/bin/env groovy

def call(steps = [], stepName = 'build', status = 'pending') {
  def utils = new pb.gitlab.utils()

  if (!utils.isFinalBuildStatus(status)) {
    steps.add(stepName)
  }
  updateGitlabCommitStatus name: stepName, state: status
}
