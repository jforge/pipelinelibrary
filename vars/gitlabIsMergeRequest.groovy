#!/usr/bin/env groovy

def call() {
  return (env.gitlabMergeRequestId != null)
}
