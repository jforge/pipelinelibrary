#!/usr/bin/env groovy

def buildImage(imageId, dockerFilePath, stageName = 'image') {
  def buildArgs = "--no-cache --rm ${dockerFilePath}"

  return {
    stage("Build ${stageName}") {
      echo "Build image: ${imageId} with dockerfile ${dockerFilePath}"
      docker.build(imageId, buildArgs)
    }
  }
}

def call(Map input = [:]) {
  def config = [user: '', dockerImages: [], outBuildTasks: [:]]
  config << input

  def utils = new pb.docker.utils()

  for(itJob in config.dockerImages) {
    def imageId = utils.getTaggedBuildJobImageId(config.user, itJob.imageName)
    config.outBuildTasks[itJob.imageName] = buildImage(imageId, itJob.dockerfilePath, itJob.imageName)
  }

  return config.outBuildTasks
}
