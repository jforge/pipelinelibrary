def call(defaultBranch = '') {

  if (env.gitlabTargetBranch) {
    return env.gitlabTargetBranch
  }

  return defaultBranch
}
