#!/usr/bin/env groovy

def call(Map input = [:]) {
  def config = [name: '']
  config << input

  def repoLocal = new pb.repo.local()

  if (config.name != '') {
    return repoLocal.isCommitOnBranch(config.name, repoLocal.getCurrentCommitId())
  }

  return false
}
