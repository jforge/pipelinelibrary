#!/usr/bin/env groovy

def call(steps = [], stepName = 'build', status = 'pending') {
  def utils = new pb.gitlab.utils()

  if (!steps.contains(stepName)) {
    steps.add(stepName)
  }
  if (utils.isFinalBuildStatus(status)) {
    steps.remove(stepName)
  }
  updateGitlabCommitStatus name: stepName, state: status
}
